package com.linearalgebra;

public class application {
    public static void main(String[] args) {
        vector3d myVector = new vector3d(1, 2, 3);
        vector3d secondVector = new vector3d(4, 5, 6);

        System.out.println(myVector.add(secondVector).getX());
        System.out.println(myVector.add(secondVector).getY());
        System.out.println(myVector.add(secondVector).getZ());
        
        System.out.println(myVector.magnitude());
        System.out.println(myVector.dotProduct(secondVector));
    }
}
