//Samir Abo-Assfour
//2137408
package com.linearalgebra;

public class vector3d {
    private double x;
    private double y;
    private double z;

    public vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }

    public double dotProduct(vector3d secondVector) {
        return this.x*secondVector.getX() + this.y*secondVector.getY() + this.z*secondVector.getZ();
    }

    public vector3d add(vector3d thirdVector3d){
        vector3d newVector = new vector3d(this.x + thirdVector3d.getX(), this.y + thirdVector3d.getY(), this.z + thirdVector3d.getZ());
        return newVector;
    }
}
