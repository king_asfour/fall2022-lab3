//Samir Abo-Assfour
//2137408
package com.linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class vector3dTest {
    @Test
    public void getMethodsTests() {
        vector3d testVector = new vector3d(5, 7, 9);

        assertEquals(5.0, testVector.getX(), 0);
        assertEquals(7.0, testVector.getY(), 0);
        assertEquals(9.0, testVector.getZ(), 0);
    }

    @Test
    public void testMagnitude() {
        vector3d testVector = new vector3d(5, 7, 9);

        assertEquals(12.4498996, testVector.magnitude(), 0.001);
    }

    @Test
    public void testDotProduct() {
        vector3d testVector1 = new vector3d(5, 7, 9);
        vector3d testVector2 = new vector3d(1, 3, 4);

        assertEquals(62, testVector1.dotProduct(testVector2), 0);
    }

    @Test
    public void testAddMethod() {
        vector3d testVector1 = new vector3d(5, 7, 9);
        vector3d testVector2 = new vector3d(1, 3, 4);

        assertEquals( 6.0, testVector1.add(testVector2).getX(),0);
        assertEquals(10, testVector1.add(testVector2).getY(),0);
        assertEquals(13, testVector1.add(testVector2).getZ(),0);
        
    }
}
